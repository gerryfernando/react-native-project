

const TimeSchedule= {
	getDate : function(){
		let today = new Date();
		let dd = String(today.getDate()).padStart(2, '0');
		let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		let yyyy = today.getFullYear();
		//2019-08-25
		return yyyy + '-'+ mm + '-' + dd ;
	}
	,
	parseDate:function(str){
		
		let res = str.split("-");
		return {year : res[0] , month: res[1] , day : res[2]}
		
	}

}

export default TimeSchedule