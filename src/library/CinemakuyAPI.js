

const CinemakuyAPI= {
	// Login function
	// https://gitlab.com/impal-group-1/project-tibiko#login-user-account-get
	Login : function(body,callback){
		
		let val = fetch('https://cinemakuy.herokuapp.com/account/login',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			responseJson.username = body.username;
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	UpdateAccount : function(body,callback){
		console.log(body);
		let val = fetch('https://cinemakuy.herokuapp.com/account/update',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json; charset=UTF-8"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});
	},
	ChangePass : function(body,callback){
		console.log(body);
		let val = fetch('https://cinemakuy.herokuapp.com/account/updatepass',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json; charset=UTF-8"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});
	},
	GetAccount : function(body,callback){
		console.log(body);
		let val = fetch('https://cinemakuy.herokuapp.com/account/get',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json; charset=UTF-8"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			console.log(responseJson);
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});
	},
	
	Register : function(body,callback){
		
		let val = fetch('https://cinemakuy.herokuapp.com/account/registration',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	
	LoginCheck : function(body,callback){
		
		let val = fetch('https://cinemakuy.herokuapp.com/account/get',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson.isSuccess);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	GetUserData : function(body,callback){
		
		let val = fetch('https://cinemakuy.herokuapp.com/account/get',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	GetUserTicket : function(body,callback){
		
		let val = fetch('https://cinemakuy.herokuapp.com/account/ticket',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	BuyTicket : function(body,callback){

		let val = fetch('https://cinemakuy.herokuapp.com/movies/buyTicket',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	getMovieNowPlaying : function(body,callback){
		
		
		let url = 'https://cinemakuy.herokuapp.com/movies/now_playing'
		if(body != null && body != ""){
			url += '?city='+body
		}
		console.log(url);
		let val = fetch(url)
		.then((response) => response.json())
		.then((responseJson) => {
			
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},//
	getMovieSchedule : function(body,callback){
		
		
		let url = "https://cinemakuy.herokuapp.com/movies/schedule?event_id="+body.event_id+"&city="+body.city+"&date="+body.date
		
		console.log(url);
		
		let val = fetch(url)
		.then((response) => response.json())
		.then((responseJson) => {
			
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	getSeatsStructure : function(body,callback){
		
		
		let url = "https://cinemakuy.herokuapp.com/movies/schedule/"+body+"/seats"
		
		console.log(url);
		
		let val = fetch(url)
		.then((response) => response.json())
		.then((responseJson) => {
			
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	createSessionTopUp : function(body,callback){
		
		let url = "https://cinemakuy.herokuapp.com/payment/cstore"
		
		let val = fetch(url,{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	GetUserPaymentOrders : function(body,callback){
		
		let val = fetch('https://cinemakuy.herokuapp.com/account/orders',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},
	DeleteUserPaymentOrders : function(body,callback){
		
		let val = fetch('https://cinemakuy.herokuapp.com/account/cancelorders',{
		  method: "POST",
		  body: JSON.stringify(body),
		  headers: {
			"Content-Type": "application/json"
		  }
		})
		.then((response) => response.json())
		.then((responseJson) => {
			callback(responseJson);
		})
		.catch((error) => {
			console.error(error);
		});

	},

}

export default CinemakuyAPI