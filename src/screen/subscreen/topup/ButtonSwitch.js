import React, { Component } from 'react';
import {Text , View,TouchableOpacity} from 'react-native';
import { Image , Button } from 'react-native-elements';
import Styles from '../../style/TopUpStyle';

class ButtonSwitch extends Component {
	
	
	
	render() {
		
		console.log(this.props.provider);
		//this.props.onChangeProvider();
		return (
				
					<View style={{ 	width : '100%' , flexDirection: 'row' , height:Styles.main.screenHeight*0.3 , justifyContent: "center", alignItems: "center" }}
					>
					
						<Button title={"Indomaret"} 
								onPress={()=>{ if(this.props.provider!="Indomaret"){ this.props.onChangeProvider('Indomaret'); }  }} 
								buttonStyle={ (this.props.provider=="Indomaret") ? Styles.styleSheets.styleActiveTab : Styles.styleSheets.styleUnactiveTab }
								titleStyle={{fontFamily:Styles.main.fonts.fontOpenSans}}
						></Button>
						<Button title={"Alfamart"} 
								onPress={()=>{ if(this.props.provider!="Alfamart"){ this.props.onChangeProvider('Alfamart'); }  }}
								buttonStyle={ (this.props.provider=="Alfamart") ? Styles.styleSheets.styleActiveTab : Styles.styleSheets.styleUnactiveTab }
								titleStyle={{fontFamily:Styles.main.fonts.fontOpenSans}}
						></Button>
					
					</View>
				)					
	}
	
}

export default ButtonSwitch
