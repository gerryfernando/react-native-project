import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';
import { StackNavigator } from 'react-navigation';
import {Text, View,ImageBackground,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Images from '../../../library/images';
import Style from '../../style/HomePageStyle';


class LoadingSideBar extends Component {
	
	
	
	render(){
		
		/*<ImageBackground
					 source={Images.profileBg}
					style={{width:'50%', height:'50%'}}
					/>*/
		
		return(
			<View>
			
				<View style={{position:'absolute',width:Style.main.screenWidth*(6/7),height:Style.main.screenWidth*(6/7)*0.6,backgroundColor:'#000000'}}>
					<ImageBackground
					 source={Images.profileBg}
					style={{width:'100%', height:'100%'}}
					/>
				</View>
				<View style={{blur:'70%',height:'100%',alignItems:'center',justifyContent:'center' backgroundColor:'#ecf0f1'}}>
					
						
				</View>
				
			
			</View>
		);
	}
	
	
}

GuestSideMenu.propTypes = {
  navigation: PropTypes.object
};

export default LoadingSideBar;