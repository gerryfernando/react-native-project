import React, { Component } from 'react';
import { Text, View, Button } from 'react-native';

class HelloWorldApp extends Component {
	
	constructor(props) {
	  super(props);
	  // Don't call this.setState() here!
	  this.navigate = props.navigation;
	}
	render() {
		return (
		  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
			<Text>Hello, world!2</Text>
				
		  </View>
		);
	}
}

export default HelloWorldApp