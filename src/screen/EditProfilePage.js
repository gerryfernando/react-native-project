import React, { Component, Fragment } from 'react';
import { Alert, Text, View,ImageBackground, Image,StatusBar} from 'react-native';
import { Button, Input, Icon,Header} from 'react-native-elements';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Images from '../library/images';
import CinemakuyAPI from '../library/CinemakuyAPI';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';
import Style from './style/RegisterPageStyle';
import { StackNavigator} from  'react-navigation';


class EditProfilePage extends Component {
	
	constructor(){
		super();
		this.UpdateProcessed = this.UpdateProcessed.bind(this);
	}
	static navigationOptions = {
		header: null ,
	}
	
	state = {
		loading:false,
		udata:null,
		name:"",
		phone:"",
		email:"",
		pass:""
    }
	
	getAccount(username,token){
		console.log("masuk1");
		CinemakuyAPI.GetAccount({username,token} , (response)=>{
			console.log("Data Get Finish");
			console.log(response);
			this.setState({udata : response});
			this.setState({
				name:response.name,
				phone:response.phone,
				email:response.email
			});
		});
	}
	componentDidMount() {
		AsyncStorage.getItem('account/username',(err, result) => {
			username = result;
			AsyncStorage.getItem('account/token',(err, result) => {
				token = result;
				this.getAccount(username , token);
			} )
		} )
	}
	
	Validate(val){
		
		if(this.state.name == ""){
			return "Name Field is empty"
		}else if(this.state.email == ""){
			return "Email Field is empty"
		}else if(this.state.phone == ""){
			return "Phone Field is empty"
		}else if(this.state.pass == ""){
			return "Password Field is empty"
		}else if(this.state.email!=""){
					if (!this.state.email.includes("@")){
						return "invalid email input"
					}
		}else if(this.state.phone!="") {
			if (!(this.state.phone.substring(0, 2) == "08") && !(this.state.phone.substring(0, 1) == "+")) {
				return "unrecognize phone number"
			}
		}
		
		return ""
	}
	
	onNameChange(name){
		console.log("name : ",name)
		if(name==""||name == null){
			this.setState({nama:this.state.udata.name});
		}else{
			this.setState({name:name});
		}
		
	}
	onEmailChange(email){
		console.log("email : ",email)
		if(email==""||email==null){
			this.setState({email:this.state.udata.email});
		}else{
			this.setState({email:email});
		}
		
	}
	onPhoneChange(phone){
		console.log("phone : ",phone)
		if(phone==""||phone==null){
			this.setState({phone:this.state.udata.phone});
		}else{
			this.setState({phone:phone});
		}
		
	}
	
	
	CinemakuyUpdateProcess(){
		
		let validateResult = this.Validate(this.state);
		if(validateResult != ""){
			Alert.alert("Invalid Input" , validateResult);
			return
		}
		this.setState({loading:true});
		let request = {name : this.state.name ,username : username, email : this.state.email , 
						pass : this.state.pass ,phone :this.state.phone,token:token}

		console.log(request);
		
		CinemakuyAPI.UpdateAccount(request , this.UpdateProcessed );
		
	}
	
	UpdateProcessed(response){
		console.log(response);
		if(response.isSuccess){
			ToastAndroid.showWithGravity(
			  'Edit Profile Berhasil',
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
			console.log("cek1 " ,response.isSuccess );
			this.props.navigation.navigate('Home');
			
		}else{
			//this.props.navigation.navigate('Home');
			console.log("cek2 " ,response.respon );
			ToastAndroid.showWithGravity(
			  'Maaf Edit Profile Gagal: '+response.respon,
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
			
			this.setState({loading:false})
		}
	}
	
	render() {

		return (
		
			
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
			
				<StatusBar
					backgroundColor='#313C4C'
				/>
				<View style={Style.styleSheets.formContainer} >
					<ImageBackground
					  source={Images.registerBg}
					  style={Style.styleSheets.fullScreen}
					/> 
				</View>
				<View style={Style.styleSheets.formContainer} >
				
					<View style={{width:'100%',alignItems: 'center',}}>
					
					<Text style={{color:'#ffffff',fontFamily:Style.main.fonts.fontElegance,fontSize:30,marginTop:20,letterSpacing:5}}>Edit Profile</Text>
					
					<Input
						  placeholder='username'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({username:value}) }}
						  editable={false}	
						  value={username}
						  leftIcon={
							<Icon
							  name='person-pin'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					<Input
						  placeholder='name'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({name:value}) }}
						  value={this.state.name}
						  leftIcon={
							<Icon
							  name='person'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					
					<Input
						  placeholder='email'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({email:value}) }}
						  value={this.state.email}
						  leftIcon={
							<Icon
							  name='mail'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					<Input
						  placeholder='phone'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({phone:value}) }}
						  value={this.state.phone}
						  leftIcon={
							<Icon
							  name='call'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					
					<Input
						  placeholder='Password'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({pass:value}) }}
						  secureTextEntry={true} 
						  						  
						  leftIcon={
							<Icon
							  name='lock'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>			
					
					<Button 
					  buttonStyle={Style.styleSheets.loginButton}
					  onPress={() => {this.CinemakuyUpdateProcess();}}
					  loading={this.state.loading}
					  title="CONFIRM" titleStyle={{color:'#FD6C57',fontFamily:Style.main.fonts.fontElegance}}
					  disabled={this.state.loading}
					></Button>
					
					
				</ View>
					
			</View>
		</View>
			
		);
	}
}

export default EditProfilePage