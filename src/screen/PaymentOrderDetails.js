import React, { Component } from 'react';
import { Text, View , ScrollView } from 'react-native';
import CinemakuyAPI from '../library/CinemakuyAPI';
import Style from './style/TopUpStyle';
import {Button , Image} from 'react-native-elements';

class PaymentOrderDetails extends Component {
	static navigationOptions = {
		header: null ,
	}
	componentWillMount() {
		//this.props.navigation.state.params
		console.log(this.props.navigation.state.params);
		let status = ''
		switch(this.props.navigation.state.params.tstatus){
			case -1 :
				status = 'Pending'
			break;
			case 0 :
				status = 'Failed'
			break;
			case 1 : 
				status = 'Success'
			break;
		}
		let response = {channel: this.props.navigation.state.params.via,
						desc: this.props.navigation.state.params.desc,
						created : this.props.navigation.state.params.date,
						expired: this.props.navigation.state.params.expired,
						kode_pembayaran: this.props.navigation.state.params.ref, 
						status : status,
						count :this.props.navigation.state.params.count }
		this.state.response = response;
	}
	moneyToCurency(money) {
		money = money+"";
		let extra = money.includes("-")?1:0;
		pointCount = (money.length-1-extra) / 3;
		let panjang = money.length;
		for(let i = 1 ; i <= pointCount; i++){
			let len = panjang - (3*i);
			money = money.slice(0, len) + "." + money.slice(len);
		}

		return money;
	}
	state = {
		response : null
	}
	render() {
		console.log(this.state.response);
		let img = null

		if(this.state.response != null){
			let imgUri = this.state.response.channel == 'indomaret' ? 'https://indomaret.co.id/logo_indomaret.png': 'https://upload.wikimedia.org/wikipedia/commons/9/9e/ALFAMART_LOGO_BARU.png';
			img = (<Image 
						source={{uri : imgUri}}
						style={{ width: '100%', height: '100%'}}
					>
				</Image>)
		}
		return (
		  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
			<ScrollView>
				<View style={{height :Style.main.screenHeight*0.2 , width:Style.main.screenWidth , marginTop : Style.main.screenHeight*0.05}}>
					{img != null && img}
				</View>
				
				<View style={{width:Style.main.screenWidth , marginTop : 10 , backgroundColor:'#35536F', borderTopRightRadius : 30 , borderTopLeftRadius : 30  , paddingBottom : 10 , justifyContent:'center', alignItems:'center'}}>
					<Text style={{fontSize: 24,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , marginTop : 5 , color:'#FFFFFF'}}>Payment Details</Text>
					
					<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , color:'#FFFFFF'}}>Payment Provider</Text>
						<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{this.state.response.channel}</Text>
					</View>
					<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , color:'#FFFFFF'}}>Status</Text>
						<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{this.state.response.status}</Text>
					</View>
					
					<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , color:'#FFFFFF'}}>Description</Text>
						<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{this.state.response.desc}</Text>
					</View>
					<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans, color:'#FFFFFF'}}>Payment Code</Text>
						<Text style={{fontSize: 14,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{this.state.response.kode_pembayaran}</Text>
					</View>
					<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , color:'#FFFFFF'}}>Amount</Text>
						<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>Rp. {this.moneyToCurency(this.state.response.count)}</Text>
					</View>
					<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10 , borderRadius : 10}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans, color:'#FFFFFF'}}>Created</Text>
						<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{this.state.response.created} {this.state.response.expired.split(' ')[1]} WIB</Text>
					</View>
					<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10 , borderRadius : 10}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans, color:'#FFFFFF'}}>Expired</Text>
						<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{this.state.response.expired}</Text>
					</View>
					<Button 
						buttonStyle={{width:Style.main.screenWidth*0.85 , height:50 , marginTop : 10 , backgroundColor : '#FFFFFF'}}
						title={"OK , Let's pay!"}
						titleStyle={{color:'#FD6C57'}}
						onPress={()=>{
							this.props.navigation.pop();}}
						>
					</Button>
				</View>
			</ScrollView>
		  </View>
		);
	}
}

export default PaymentOrderDetails