import React, { Component, Fragment } from 'react';
import { Alert, Text, View,ImageBackground, Image,StatusBar} from 'react-native';
import { Button, Input, Icon } from 'react-native-elements';
//import Icon from 'react-native-vector-icons/FontAwesome';
import Images from '../library/images';
import CinemakuyAPI from '../library/CinemakuyAPI';
import Style from './style/LoginPageStyle';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';


class LoginPage extends Component {
	constructor(){
		super();
		this.onLoginProcessed = this.onLoginProcessed.bind(this);
		console.log(Style.main.screenWidth);
		console.log(Style.main.screenHeight);
	}
	static navigationOptions = {
		header: null ,
	};
	
	CinemakuyLoginProcess(){
		
		// condition check
		if(this.state.user.value == "" || this.state.pass.value == ""){
			Alert.alert("Invalid Input","Username or Password is empty");
			return
		}
		console.log("Login processing")
		let usernameB = this.state.user.value;
		let passwordB = this.state.pass.value;
		
		this.setState({loading : true});
		console.log({username: usernameB , pass: passwordB})
		ToastAndroid.showWithGravity(
		  'Login in Process',
		  ToastAndroid.SHORT,
		  ToastAndroid.BOTTOM,
			25,
			50,
		);
		CinemakuyAPI.Login( {username: usernameB , pass: passwordB} , this.onLoginProcessed )
		
	}
	
	onLoginProcessed(response){
		console.log("Login processed")
		console.log(response)
		this.setState({loading : false});
		if(response.isSuccess){
			ToastAndroid.showWithGravity(
			  'Welcome To Cinemakuy',
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
			AsyncStorage.setItem('account/token', response.token);
			AsyncStorage.setItem('account/username', response.username);
			this.props.navigation.navigate('Home');
		}else{
			ToastAndroid.showWithGravity(
			  ''+response.respon,
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
		}
		
		
	}
	
	loginGuest(){
		AsyncStorage.setItem('account/token', "");
		AsyncStorage.setItem('account/username', "");
	}
	
	onPassChange(pass){
		
		if(pass.length <= 1){
			this.state.pass.error = "Password must have min. 6 character"
			return
		}
		this.state.pass.error = ""
		this.state.pass.value = pass
		//console.log(this.state.user.error != "" || this.state.pass.error != "")
	}
	
	onUserChange(user){
		
		if(user.length <= 1){
			this.state.user.error = "Username must have min. 6 character"
			return
		}
		this.state.user.error = ""
		this.state.user.value = user
		//console.log(this.state.user.error != "" || this.state.pass.error != "")
	}
	state = {
        user: {
            error: " ",//one space has used to disable the button for the first time
            value: ""//to store email address
        },
		pass: {
            error: " ",//one space has used to disable the button for the first time
            value: ""//to store email address
        },
		loading:false,
		checking : true
    }
	//disableValid(){return (this.state.user.error != "" || this.state.pass.error != "")}
	
	CinemakuyLoginChecking(username, token){
		let request = {username : username , token : token}
		CinemakuyAPI.LoginCheck(request , (isSuccess)=>{
			if(isSuccess){
				ToastAndroid.showWithGravity(
				  'You already logged in',
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
				this.props.navigation.navigate('Home');
			}
			this.setState({checking : false});
			
		})	
	}
	
	componentDidMount() {
		
		
		ToastAndroid.showWithGravity(
		  'Login Status Checking...',
		  ToastAndroid.SHORT,
		  ToastAndroid.BOTTOM,
			25,
			50,
		);
		AsyncStorage.multiGet(['account/token','account/username'], (err, result) => {
			if(result[0][1] != "" && result[1][1]!=""){
				//checking=true;
				//this.setState({checking : true});
				console.log("Check Account Login Status");
				this.CinemakuyLoginChecking(result[1][1],result[0][1]);
			}else{
				this.setState({checking : false});
			}
		});
		
	}
	
	render() {
		
		
		var checking=this.state.checking;
		
		return (
			
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<StatusBar
					backgroundColor='#0080C1'
				/>
				<View style={Style.styleSheets.formContainer} >
					<ImageBackground
					  source={Images.loginBg}
					  style={Style.styleSheets.fullScreen}
					/> 
				</View>
				<View style={Style.styleSheets.formContainer} >
				
					<View style={{marginTop:-Style.main.screenHeight/20 , width:Style.main.screenWidth , alignItems: 'center'}}>
						<Text style={{color:'#ffffff',fontFamily:Style.main.fonts.fontElegance,fontSize:30,marginTop:30,letterSpacing:5}}>WELCOME TO</Text>
						<Text style={{color:'#ffffff',fontFamily:Style.main.fonts.fontElegance,fontSize:20}}>CINEMAKUY</Text>
						
						<Image
						  source={Images.logo}
						  style={{ width: Style.main.screenWidth/3, height: Style.main.screenWidth/3, marginTop:Style.main.screenHeight/64}}
						/>
					</View>
					
					<View style={{marginTop:Style.main.screenHeight/16,width:Style.main.screenWidth,alignItems: 'center',}}>
						<Input
							  placeholder='Username'
							  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
							  containerStyle={{width:'68%',marginBottom:15}}
							  inputContainerStyle={{ borderBottomColor: '#FD6C57'}}
							  placeholderTextColor='#FD6C57'
							  onSubmitEditing={() => this.passwordRef.focus()}
							  
							  errorMessage={this.state.user.error}
							  onChangeText={(value) => {this.onUserChange(value); /*this.setState({user:{error:""}});*/} }
							  
							  leftIcon={
								<Icon
								  name='person-pin'
								  size={20}
								  color='#FD6C57'
								  style={{marginLeft:-10}}
								/>
							  }
							/>
							
						<Input
						  placeholder='Password'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  ref={ref => this.passwordRef = ref}
						  secureTextEntry={true} 
						  
						  errorMessage={this.state.pass.error}
						  onChangeText={(value) => {this.onPassChange(value); /*this.setState({pass:{error:""}})*/} }
						  
						  leftIcon={
							<Icon
							  name='lock'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10 , marginBottom : 20}}
							/>
						  }
						/>
					
					
					
					
					
						<Button 
						  buttonStyle={Style.styleSheets.loginButton}
						  onPress={() => {this.CinemakuyLoginProcess(); this.loading=true;}}
						  loading={this.state.loading}
						  title="LOG IN" titleStyle={{color:'#FD6C57',fontFamily:Style.main.fonts.fontElegance}}
						  disabled={checking}
						></Button>
						
					
					<Text style={{color:'#ffffff',fontFamily:Style.main.fonts.fontElegance,fontSize:10,margin:20,marginBottom:0}}>
					Not a member? 
					<Text> </Text>
					<Text style={{color:'#FD6C57',fontFamily:Style.main.fonts.fontElegance,fontSize:10,margin:20,marginBottom:0,textDecorationLine: 'underline'}}
					
					onPress={()=>{ 
						console.log("Register Page");
						this.props.navigation.navigate('Register');
					}}
					>
					Register Now
					</Text></Text>
					<Text style={{color:'#fdfdfd',fontFamily:Style.main.fonts.fontElegance,fontSize:15,margin:20,marginBottom:0,textDecorationLine: 'underline'}}
					
					onPress={()=>{ 
						console.log("Register Page");
						this.loginGuest();
						this.props.navigation.navigate('Home');
					}}
					>
						(Login as Guest)
					</Text>
				</ View>
					
			</View>
		</View>
			
		);
	}
}

export default LoginPage