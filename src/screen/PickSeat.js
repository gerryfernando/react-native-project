import React, { Component } from 'react';
import { Text, View ,ScrollView ,ToastAndroid} from 'react-native';
import { Button , Icon} from 'react-native-elements';
import Icon2 from 'react-native-vector-icons/EvilIcons';
import CinemakuyAPI from '../library/CinemakuyAPI';
import Style from './style/DetailPageStyle';

class PickSeat extends Component {
	
	constructor(props) {
	  super(props);
	  // Don't call this.setState() here!
	  this.navigate = props.navigation;
	  //this.rendering = this.rendering.bind(this);
	}
	
	static navigationOptions = {
		header: null ,
	}
	
	componentDidMount() {
		
		
		CinemakuyAPI.getSeatsStructure(this.props.navigation.state.params.schedule_id , (response)=>{
			console.log("Pick Seat schedule Id");
			
			if(response.status == 200){
				console.log("item loaded");
				//console.log(response);
				this.setState({seats:response , audi:response.data.audi.replace('#','')});
				//this.generateSeats();
				//
			}else{
				ToastAndroid.showWithGravity(
				  response.message,
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
			}
			//console.log(this.state.seats);
		});
		return true;
	}
	state = {
		seats : null,
		reload : true,
		selected : [],
		rendering : false
	}
	
	
	
	generateSeats(){
		if(this.state.seats == null){
			return (<Text> Loading... </Text>);
		}
		let seats = [];
		for(let i=0; i < this.state.seats.data.seat_layout.length; i++){
			//console.log("Render Part",i);
			seats.unshift(
				<View key={i} style={{backgroundColor:'#2D3E50',justifyContent:'center',alignItems:'center',margin:10,marginTop : 30,borderRadius:10,paddingBottom:20}}>
					<Text style={{ margin:30 , color:'#FFFFFF'}}>{this.state.seats.data.seat_layout[i].section_name} - Rp.{this.state.seats.data.seat_layout[i].price}</Text>
					
					{this.generateSeatPart(this.state.seats.data.seat_layout[i])}
					
				</View>
			)
		}
		seats.unshift(
			<View key={-1} style={{backgroundColor:'#2D3E50',justifyContent:'center',alignItems:'center',marginBottom:10,marginTop : 0,marginBottom:20,borderBottomLeftRadius:200,borderBottomRightRadius:200}}>
				<Text style={{ margin:20 ,fontFamily:Style.main.fonts.fontOpenSans ,fontWeight:'bold',fontSize:18, color:"#FFFFFF" }}>Movie Screen</Text>
			</View>
		)
		//console.log(seats);
		return seats;
		
	}
	pick(item){
		if(this.state.selected.length >= 6){
			ToastAndroid.showWithGravity(
				  "You only can book 6 seats",
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
			return false;
		}
		this.state.selected.push(item);
		return true;
	}
	release(item){
		var index = this.state.selected.indexOf(item);
		if (index !== -1){ 
			this.state.selected.splice(index, 1);
			return true;
		}else{
			return false;
		}
	}
	
	generateSeatPart(seatLayout){
		let seats = [];
		//console.log(this.state.seats);
		let count = 0;
		let col = seatLayout.seats[0].column;
		let total = seatLayout.seats.length;
		let pointHelper = {p1 : 0 , p2:0}
		while(count < total){
			//console.log("generate : ",col);
			seats.unshift(
				<View key={col} style={{backgroundColor:'#2D3E50' , flexDirection: 'row' , minHeight:20 }}>
					{	this.getColumnData(col,pointHelper).map((item,j) =>{
							count++;
							//console.log("generate : ",item.code);
							if(item.status == -1){
								return <View key={j} style={{height:38,width:38, margin : 2}}></View>
							}else{
								//console.log(item.color);
								let itmCol = item.color==null?'#FD6C57':item.color;
								return(
									<View key={j} style={{height:38,width:38, margin : 2}}>
										<Button 
											disabled={item.status==1}
											buttonStyle = {{backgroundColor: itmCol ,height:35,width:38 }}
											title={item.code}
											titleStyle={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:10}}
											onPress={() => this.clickSeat(item)}
										></Button>	
									</View>
								)
							}
						})
					}
				</View>)
			col++;
		}
		return seats;
	}
	
	clickSeat(item){
		if(this.state.rendering){
			return;
		}
		this.state.rendering = true;
		if(item.color == null || item.color == '#FD6C57'){
													
			if(this.pick(item)){
				item.color = '#58db7b';
				//console.log("item inserted");
				this.setState({reload: !this.state.reload});
			}
		}else{
			
			if(this.release(item)){
				item.color = '#FD6C57';
				//console.log("item uninserted");
				this.setState({reload: !this.state.reload});
			}
		}
	}
	getTotalSeat(){
		count = 0;
		for(let i = 0 ; i <this.state.seats.data.seat_layout.length; i++){
			count += this.state.seats.data.seat_layout[i].seats.length;
		}
		return count;
	}
	getColumnData(colNumber , pointHelper){
		column = [];
		let breakTime = false;
		for(let i = pointHelper.p1 ; i <this.state.seats.data.seat_layout.length; i++){
			for(let j = pointHelper.p2; j < this.state.seats.data.seat_layout[i].seats.length; j++){
				if(this.state.seats.data.seat_layout[i].seats[j].column == colNumber){
					column.unshift(this.state.seats.data.seat_layout[i].seats[j]);
				}else if(this.state.seats.data.seat_layout[i].seats[j].column > colNumber){
					pointHelper.p1 = i;
					pointHelper.p2 = j;
					breakTime = true;
					break;
				}
			}
			if(breakTime){
				break;
			}
		}
		return column;
	}
	
	checkout(){
		if(this.state.selected.length<1){
			ToastAndroid.showWithGravity(
				  "You're not pick seat yet",
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
			return;
		}
		//audi,schedule_id , orders , provider , date , time
		let data = {
			time : this.props.navigation.state.params.time,
			date : this.props.navigation.state.params.date,
			schedule_id:this.props.navigation.state.params.schedule_id
		}
		this.props.navigation.navigate('Checkout' , {audi:this.state.audi,orders:this.state.selected,provider:this.props.navigation.state.params.provider,other : data});
	}
	
	
	render() {
		//console.log("render1");
		let kursi = this.generateSeats();
		//console.log("render2");
		//console.log(this.props.navigation.state.params.provider.name);
		this.state.rendering = false;
		return (
		  <View style={{ flex: 1 }}>

				<View style={{justifyContent: "center", alignItems: "center" ,height:Style.main.screenHeight*0.2 ,width:Style.main.screenWidth ,backgroundColor:'#2D3E50'}}>
					<View style={{ flexDirection:'row', justifyContent: "center", alignItems: "center" ,height:Style.main.screenHeight*0.15 ,width:Style.main.screenWidth*0.9,backgroundColor:'#283949',borderRadius:20}}>
						<View style={{ flex: 0.3, justifyContent: "center", alignItems: "center",borderRightWidth:1 , borderColor:'#FFFFFF' }}>
							<Icon
								name='movie'
								color='#fd6c57'
							/>
							<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold' ,fontSize:12, color:"#FFFFFF" }} >{this.props.navigation.state.params.provider.name}</Text>
						</View>
						<View style={{ flex: 0.3, justifyContent: "center", alignItems: "center" }}>
							<Icon2
								name='clock'
								color='#fd6c57'
								size={30}
							/>
							<Text style={{fontFamily:Style.main.fonts.fontOpenSans ,fontWeight:'bold',fontSize:12, color:"#FFFFFF" }} >{this.props.navigation.state.params.time}</Text>
						</View>
						<View style={{ flex: 0.3, justifyContent: "center", alignItems: "center",borderLeftWidth:1 , borderColor:'#FFFFFF'  }}>
							<Icon2
								name='calendar'
								color='#fd6c57'
								size={30}
							/>
							<Text style={{fontFamily:Style.main.fonts.fontOpenSans ,fontWeight:'bold',fontSize:12, color:"#FFFFFF" }} >{this.props.navigation.state.params.date}</Text>
						</View>
					</View>
					
				</View>
				
				<View style={{backgroundColor:'#DEDEDE',width:Style.main.screenWidth , height:Style.main.screenHeight*0.67,justifyContent:'center',alignItems:'center'}}>
					<ScrollView horizontal>
						<ScrollView >
							<View style={{backgroundColor:'#DEDEDE'}}>
								{kursi}
							</View>
						</ScrollView>
					</ScrollView>
				</View>
				<View style={{backgroundColor:'#999999',width:Style.main.screenWidth , height:Style.main.screenHeight*0.1}}>

				</View>
				<View style={{position:'absolute',bottom:0, backgroundColor:'#999999',width:Style.main.screenWidth , height:Style.main.screenHeight*0.1,justifyContent:'center',alignItems:'center'}}>
						<Button 
							disabled={this.state.seats==null}
							buttonStyle={{width:Style.main.screenWidth,height:'100%'}}
							title={"Buy Seats"}
							onPress={() => {
								this.checkout();
							}}
						></Button>
				</View>
		  </View>
		);
	}
}

export default PickSeat