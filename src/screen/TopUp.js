import React, { Component } from 'react';
import { Text, View,  ScrollView } from 'react-native';
import ButtonSwitch from './subscreen/topup/ButtonSwitch';
import Style from './style/TopUpStyle';
import { Input , Button , Image} from 'react-native-elements';

class TopUp extends Component {
	
	constructor(props) {
	  super(props);
	  this.navigate = props.navigation;
	}
	
	static navigationOptions = {
		header: null ,
	}
	
	state = {
		provider : "Indomaret",
		credit : ""
		
	}
	
	onChanged(text){
		let newText = '';
		let numbers = '0123456789';
		if(this.state.credit=="" && text == '0'){
		alert("0 can't be first value");
			this.setState({ credit: newText });
			return;
		}
		for (var i=0; i < text.length; i++) {
			if(numbers.indexOf(text[i]) > -1 ) {
				newText = newText + text[i];
			}
			else {
				// your call back function
				alert("please enter numbers only");
			}
		}
		this.setState({ credit: newText });
	}
	
	gotoCheckoutTopUp(){
		/*{ // sample
			price : 5000,
			channel : "indomaret"/alfamaret,
			username : "a1",
			phone : "081914830159",
			email : "firdi.ansyah20@gmail.com"	
		}*/
		if(this.state.credit=="" || parseInt(this.state.credit) < 5000 ){
			alert("Pembelian Minimal 5.000");
			return;
		}
		let body = { // sample
			price : parseInt(this.state.credit),
			channel : this.state.provider.toLowerCase(),
			username : this.props.navigation.state.params.username,
			phone : this.props.navigation.state.params.udata.phone,
			email : this.props.navigation.state.params.udata.email	
		}
		//console.log(body)
		this.props.navigation.navigate('TopUpCheckout' , body);
	}
	
	render() {
		
		
		return (
		  <View style={{ backgroundColor: '#35536F', height : Style.main.screenHeight, justifyContent: "center", alignItems: "center" }}>
			< ScrollView>
		  
		  
			<View style={{justifyContent: "center", alignItems: "center" , backgroundColor:'#283949' , width:Style.main.screenWidth , height:Style.main.screenHeight*0.15 , marginBottom : Style.main.screenHeight*0.03 , borderBottomLeftRadius : 100 , borderBottomRightRadius : 100 }} >
				<Text style={{fontSize : 28, color:'#FFFFFF', fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans}}>Top Up Saldo</Text>
			</View>
			<View style={{ justifyContent: "center", alignItems: "center"}}>
				<View style={{justifyContent: "center", alignItems: "center" , backgroundColor:'#283949' , borderRadius:15 , width:Style.main.screenWidth*0.8 , marginTop : Style.main.screenHeight*0.02 , marginBottom : Style.main.screenHeight*0.03,paddingBottom:Style.main.screenHeight*0.02}} >
					<Text style={{marginBottom: Style.main.screenHeight*0.02 , marginTop: Style.main.screenHeight*0.02,color:'#FFFFFF',fontFamily:Style.main.fonts.fontOpenSans }}>Credit Top Up Amount</Text>

					<Input
						placeholder={'Input top up amount'}
						inputStyle={{color:'#FFFFFF'}}
						placeholderTextColor='#DEDEDE'
						keyboardType='numeric'
						value={this.state.credit}
						onChangeText={(text)=> this.onChanged(text)}
					/>
				</View>
				
				<View style={{justifyContent: "center", alignItems: "center" , backgroundColor:'#283949' , borderRadius:15 , width:Style.main.screenWidth*0.8 , marginTop : Style.main.screenHeight*0.02 , marginBottom : Style.main.screenHeight*0.03 , paddingBottom:Style.main.screenHeight*0.02 }} >
					<Text style={{marginBottom: Style.main.screenHeight*0.02 , marginTop: Style.main.screenHeight*0.02,color:'#FFFFFF',fontFamily:Style.main.fonts.fontOpenSans }}>Select Provider</Text>
					<ButtonSwitch provider={this.state.provider} onChangeProvider={(providerName)=>{ this.setState({provider:providerName}) }} />
					
				</View>
				
				
				<Button 
					buttonStyle={{width:Style.main.screenWidth*0.8 , borderRadius:15 , height:50, marginBottom:Style.main.screenHeight*0.03 , backgroundColor : '#FFFFFF'}}
					title={"Buy Credit"}
					titleStyle={{color:'#FD6C57'}}
					onPress={()=>this.gotoCheckoutTopUp()}
					>
				</Button>
			</View>
			
			</ ScrollView>	
		  </View>
		);
	}
}

export default TopUp