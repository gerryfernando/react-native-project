import React, { Component } from 'react';
import { Text, View,ScrollView,ToastAndroid } from 'react-native';
import Style from './style/DetailPageStyle';
import { Image , Button} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import CinemakuyAPI from '../library/CinemakuyAPI';

class Checkout extends Component {
	
	constructor(props) {
	  super(props);
	  // Don't call this.setState() here!
	  this.navigate = props.navigation;
	}
	static navigationOptions = {
		header: null ,
	}
	
	state = {
		btnDisabled : false,
		credit : 0,
		username  : "",
		token : ""
	}
	componentDidMount() {
		
		
		AsyncStorage.multiGet(['account/token','account/username'], (err, result) => {
			if(result[0][1] != "" && result[1][1]!=""){
				this.CinemakuyUserChecking(result[1][1],result[0][1]);
			}
		});
		
		//console.log(this.handleBackPress)
		return true
	}
	
	CinemakuyUserChecking(username, token){
		let request = {username : username , token : token}
		CinemakuyAPI.GetUserData(request , (json)=>{
			if(json.isSuccess){
				this.setState({credit : json.credit ,username : username , token:token });
			}
		})	
	}
	moneyToCurency(money) {
		money = money+"";
		let extra = money.includes("-")?1:0;
		pointCount = (money.length-1-extra) / 3;
		let panjang = money.length;
		for(let i = 1 ; i <= pointCount; i++){
			let len = panjang - (3*i);
			money = money.slice(0, len) + "." + money.slice(len);
		}

		return money;
	}
	buyTicket(total){
		
		body = {
			username : this.state.username,
			token : this.state.token,
			total : total,
			schedule_id : this.props.navigation.state.params.other.schedule_id,
			orders : this.props.navigation.state.params.orders,
			provider : this.props.navigation.state.params.provider,
			date : this.props.navigation.state.params.other.date,
			time : this.props.navigation.state.params.other.time,
			audi : this.props.navigation.state.params.audi
		}
		ToastAndroid.showWithGravity(
		  "Loading in purchase...",
		  ToastAndroid.SHORT,
		  ToastAndroid.BOTTOM,
			25,
			50,
		);
		console.log(body);
		CinemakuyAPI.BuyTicket(body , (response) => {
			
			if(response.isSuccess){
				ToastAndroid.showWithGravity(
				  response.respon,
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
				this.props.navigation.pop(2);
			}else{
				ToastAndroid.showWithGravity(
				  response.respon,
				  ToastAndroid.SHORT,
				  ToastAndroid.BOTTOM,
					25,
					50,
				);
			}
			this.setState({btnDisabled : false});
		});
		this.setState({btnDisabled : true});
		
	}
	
	render() {
		//console.log(this.props.navigation.state.params);
		
		let orders = [];
		let total = 0;
		
		for(let i = 0 ; i < this.props.navigation.state.params.orders.length;i++){
			//console.log("Generate List");
			
			let botWidth = i ==this.props.navigation.state.params.orders.length-1 ? 0 : 1;
			total += this.props.navigation.state.params.orders[i].price;
			orders.push(
				<View key={i} style={{width:'80%',justifyContent: "center",flexDirection:'row' , paddingTop : 10 , paddingBottom : 10 , borderBottomWidth : botWidth , borderColor : '#777777' }}>
					<View style={{width:'60%'}}>
						<View style={{backgroundColor:"#FD6C57" , borderRadius : 4 , width:50 , padding : 10 , justifyContent: "center",alignItems : 'center' }}>
							<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans, color:"#EDEDED"}}>{this.props.navigation.state.params.orders[i].code}</Text>
						</View>
					</View>
					<View style={{width:'40%'}}>
						<View style={{ paddingTop : 10 ,paddingBottom:10, justifyContent: "center" }}>
							<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans, color:"#EDEDED"}}>Rp {this.moneyToCurency(this.props.navigation.state.params.orders[i].price)} ,-</Text>
						</View>
					</View>
				</View>
			);
		}
		//console.log(orders);
		
		return (
		  <View style={{ flex: 1}}>
			<ScrollView style={{backgroundColor:'#2D3E50'}}>
				<View style={{marginTop:40 ,width:Style.main.screenWidth*0.8,marginLeft:Style.main.screenWidth*0.1, justifyContent: "center", alignItems: "center" , borderRadius : 20 , backgroundColor : '#283949'}}>
					<View style={{width:Style.main.screenWidth*0.8 , height:Style.main.screenHeight*0.1,marginTop:20,justifyContent: "center", alignItems: "center" }}>
						<View style={{ backgroundColor:"#EDEDED" , paddingLeft: 20 , paddingRight: 20,paddingTop: 10,paddingBottom: 10, borderRadius:10}}>
							<View style={{height:Style.main.screenHeight*0.1 , width:Style.main.screenHeight*0.2}}>
								<Image 
										source={{uri : this.props.navigation.state.params.provider.image }}
										style={{ width: '100%', height: '100%'}}
									>
								</Image>
							</View>
						</View>
					</View>
					<View style={{marginTop:20,justifyContent: "center", alignItems: "center"}}>
						<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , color:"#EDEDED"}}>{this.props.navigation.state.params.provider.name} - {this.props.navigation.state.params.provider.location}</Text>
						<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:15,textAlign:'center', color:"#EDEDED"}}>{this.props.navigation.state.params.provider.address}</Text>
					</View>
				</View>
				
				<View style={{marginTop:30 ,width:Style.main.screenWidth*0.8,marginLeft:Style.main.screenWidth*0.1, justifyContent: "center", alignItems: "center" , borderRadius : 20 , marginBottom:15,backgroundColor : '#283949'}}>
					<View style={{justifyContent: "center", alignItems: "center" , marginTop:20, padding:10 ,width:'80%', borderBottomWidth : 1 ,borderColor : '#777777' }}>
						<Text style={{fontSize: 18,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold' , color:"#EDEDED"}}>Orders</Text>
					</View>
					<View style={{width:'80%',justifyContent: "center",flexDirection:'row' , paddingTop : 10 , borderColor : '#777777' }}>
						<View style={{width:'60%'}}>
							<View style={{paddingTop : 10 ,paddingBottom:5, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold', color:"#EDEDED" }}>Seat Number</Text>
							</View>
						</View>
						<View style={{width:'40%'}}>
							<View style={{ paddingTop : 10 ,paddingBottom:5, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold', color:"#EDEDED" }}>Price</Text>
							</View>
						</View>
					</View>
					{orders}
					<View style={{width:'80%',justifyContent: "center",flexDirection:'row' , paddingTop : 10  ,borderTopWidth : 1 , borderColor : '#777777' }}>
						<View style={{width:'60%'}}>
							<View style={{paddingTop : 10 ,paddingBottom:5, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold' , color:"#EDEDED"}}>Total</Text>
							</View>
						</View>
						<View style={{width:'40%'}}>
							<View style={{ paddingTop : 10 ,paddingBottom:5, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold', color:"#EDEDED" }}>Rp {this.moneyToCurency(total)} ,-</Text>
							</View>
						</View>
						
					</View>
					
					<View style={{width:'80%',justifyContent: "center",flexDirection:'row' , paddingBottom : 10  }}>
						<View style={{width:'60%'}}>
							<View style={{paddingTop : 5 ,paddingBottom:10, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold', color:"#EDEDED" }}>Your Credit</Text>
							</View>
						</View>
						<View style={{width:'40%'}}>
							<View style={{ paddingTop : 5 ,paddingBottom:10, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold', color:"#EDEDED" }}>Rp {this.moneyToCurency(this.state.credit)} </Text>
							</View>
						</View>
						
					</View>
					
					<View style={{width:'80%',justifyContent: "center",flexDirection:'row' , paddingTop : 10 , paddingBottom : 10 ,borderTopWidth : 1 , borderColor : '#777777' }}>
						<View style={{width:'60%'}}>
							<View style={{paddingTop : 10 ,paddingBottom:10, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold', color:"#EDEDED" }}>Change</Text>
							</View>
						</View>
						<View style={{width:'40%'}}>
							<View style={{ paddingTop : 10 ,paddingBottom:10, justifyContent: "center" }}>
								<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,fontWeight:'bold', color:"#EDEDED" }}>Rp {this.moneyToCurency(this.state.credit-total)} ,-</Text>
							</View>
						</View>
					</View>
					<Button 
						disabled={this.state.credit < total && this.state.btnDisabled}
						buttonStyle={{width:Style.main.screenWidth*0.3 , marginBottom : 15}}
						title={"Buy"}
						onPress={() => {
							console.log("Buy");
							this.buyTicket(total);
						}}
					></Button>
					
					
				</View>
			</ScrollView >
			
		  </View>
		);
	}
}

export default Checkout