import React, { Component } from 'react';
import { Text, View ,ScrollView} from 'react-native';
import CinemakuyAPI from '../library/CinemakuyAPI';
import Style from './style/TopUpStyle';
import {Button , Image} from 'react-native-elements';

class TopUpCheckout extends Component {
	static navigationOptions = {
		header: null ,
	}
	componentDidMount() {
		console.log(this.props.navigation.state.params);
		let body = this.props.navigation.state.params;
		CinemakuyAPI.createSessionTopUp(body , (response)=>{
			// console.log(response);
			 this.setState({response :response });
		 });
		// let response = {channel: "indomaret",
						// desc: "Sukses. Silakan melakukan pembayaran melalui INDOMARET dengan menyebutkan pembayaran PLASAMALL dan kode pembayaran 241914061",
						// expired: "2019-11-22 23:47:25 WIB",
						// id: 496011,
						// kode_pembayaran: "241914061",
						// count : }
		//this.setState({response :response });
	}
	state = {
		response : null
	}
	moneyToCurency(money) {
		money = money+"";
		let extra = money.includes("-")?1:0;
		pointCount = (money.length-1-extra) / 3;
		let panjang = money.length;
		for(let i = 1 ; i <= pointCount; i++){
			let len = panjang - (3*i);
			money = money.slice(0, len) + "." + money.slice(len);
		}

		return money;
	}
	render() {
		console.log(this.state.response);
		
		let img = null
		let desc = 'Loading...'
		let paymentCode = 'Loading...'
		let expired = 'Loading...'
		let amount = 'Rp. 0'
		if(this.state.response != null){
			let imgUri = this.state.response.channel == 'indomaret' ? 'https://indomaret.co.id/logo_indomaret.png': 'https://upload.wikimedia.org/wikipedia/commons/9/9e/ALFAMART_LOGO_BARU.png';
			img = (<Image 
						source={{uri : imgUri}}
						style={{ width: '100%', height: '100%'}}
					>
				</Image>)
			desc = this.state.response.desc;
			paymentCode = this.state.response.kode_pembayaran;
			expired = this.state.response.expired;
			amount = 'Rp. '+this.moneyToCurency(this.state.response.count);
		}
		return (
		  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
		  <ScrollView>
			<View style={{height :Style.main.screenHeight*0.2 , width:Style.main.screenWidth , marginTop : Style.main.screenHeight*0.05}}>
				{img != null && img}
			</View>
			
			<View style={{width:Style.main.screenWidth , marginTop : 10 , backgroundColor:'#35536F', borderTopRightRadius : 30 , borderTopLeftRadius : 30 , paddingBottom:15 , justifyContent: "center", alignItems: "center" }}>
				<Text style={{fontSize: 24,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , marginTop : 5 , color:'#FFFFFF'}}>Payment Created</Text>
				<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
					<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans , color:'#FFFFFF'}}>Description</Text>
					<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{desc}</Text>
				</View>
				
				<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
					<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans, color:'#FFFFFF'}}>Payment Code</Text>
					<Text style={{fontSize: 14,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{paymentCode}</Text>
				</View>
				
				<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10, borderRadius : 10}}>
					<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans, color:'#FFFFFF'}}>Amount</Text>
					<Text style={{fontSize: 14,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{amount}</Text>
				</View>
				
				<View style={{ width:Style.main.screenWidth*0.85 , marginTop : 10 , backgroundColor:'#283949' , padding : 10 , borderRadius : 10}}>
					<Text style={{fontSize: 18,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans, color:'#FFFFFF'}}>Expired</Text>
					<Text style={{fontSize: 12,fontFamily:Style.main.fonts.fontOpenSans,margin:8,textAlign:'center', color:'#FFFFFF'}}>{expired}</Text>
				</View>
				<Button 
					buttonStyle={{width:Style.main.screenWidth*0.85 , height:50 , marginTop : 10 , backgroundColor : '#FFFFFF'}}
					title={"OK , Let's pay!"}
					titleStyle={{color:'#FD6C57'}}
					onPress={()=>{
						
						if(img==null){
							return;
						}
						
						alert("You could see this payment details in menu 'Payment Order' in Homepage Sidemenu");
						this.props.navigation.pop(2);}}
					>
				</Button>
			</View>
			</ScrollView>
		  </View>
		);
	}
}

export default TopUpCheckout