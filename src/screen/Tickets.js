import React, { Component } from 'react';
import { Text, View, Button,TouchableOpacity , ScrollView } from 'react-native';
import CinemakuyAPI from '../library/CinemakuyAPI';
import AsyncStorage from '@react-native-community/async-storage';
import Accordion from 'react-native-collapsible/Accordion';
import Style from './style/DetailPageStyle';
import { Image } from 'react-native-elements';

class Tickets extends Component {
	
	constructor(props) {
	  super(props);
	  // Don't call this.setState() here!
	  this.navigate = props.navigation;
	}
	
	static navigationOptions = {
		header: null ,
	}
	
	componentDidMount() {
		AsyncStorage.multiGet(['account/token','account/username'], (err, result) => {
			if(result[0][1] != "" && result[1][1]!=""){
				this.getTicket(result[1][1],result[0][1]);
			}else{
				this.props.navigation.navigate('Login');
			}
		});
	}
	
	state = {
		tickets : [],
		activeSections: []
	}
	getTicket(username , token){
		body = {username : username , token:token}
		
		CinemakuyAPI.GetUserTicket(body , (response) =>{
			if(response.isSuccess){
				
				tickets = [];
				
				for(let key in response.data){
					tickets.push(response.data[key]);
				}
				tickets.sort(function(a, b){
				  var x = a.date.toLowerCase();
				  var y = b.date.toLowerCase();
				  if (x > y) {return -1;}
				  if (x < y) {return 1;}
				  return 0;
				});
				
				this.setState({tickets : tickets });
				
			}else{
				//Failed
				
			}
		});
	}
	
	
	isDatePassed(date){
		var seperateDate = date.split("-");
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();
		if(yyyy < seperateDate[0]){
			return -1;
		}else if(mm < seperateDate[1]){
			return -1;
		}else if(dd < seperateDate[2]){
			return -1;
		}
		if(yyyy == seperateDate[0] && mm == seperateDate[1] && dd == seperateDate[2]){
			return 0;
		}
		if(yyyy > seperateDate[0]){
			return 1;
		}else if(mm > seperateDate[1]){
			return 1;
		}else if(dd > seperateDate[2]){
			return 1;
		}
		
	}
	
	_updateSections = activeSections => {
		this.setState({ activeSections });
	  };
		_renderHeader = section => {
			
			let colorDate = '#FD6C57';
			switch(this.isDatePassed(section.date)){
				case -1:
					colorDate = '#3B506B';
				break;
				case 0:
					colorDate = '#58db7b';
				break;
				case 1:
					colorDate = '#FD6C57';
				break;
			}
			
			return (
			  <View style={{ marginTop:12 , width:Style.main.screenWidth*0.80, height:50,borderBottomWidth:1 ,borderColor:'#BBBBBB',flexDirection:'row' }}>
					<View style={{width:Style.main.screenWidth*0.80*0.6 , justifyContent:'center'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333',width:Style.main.screenWidth*0.80*0.6}}>{section.cinema.name} - {section.cinema.location}</Text>
					</View>
					<View style={{width:Style.main.screenWidth*0.80*0.4 , justifyContent:'center',alignItems:'center'}}>
						<View style={{backgroundColor : colorDate  , borderRadius : 5 , padding : 5 , justifyContent:'center',alignItems:'center'}}>
							<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:10,color:'#FFFFFF'}}>{section.time} {section.date}</Text>
						</View>
					</View>
					
			  </View>
			);
		};
		  moneyToCurency(money) {
			money = money+"";
			let extra = money.includes("-")?1:0;
			pointCount = (money.length-1-extra) / 3;
			let panjang = money.length;
			for(let i = 1 ; i <= pointCount; i++){
				let len = panjang - (3*i);
				money = money.slice(0, len) + "." + money.slice(len);
			}

			return money;
		}
	  _renderContent = section => {



		  //<Text style={{color:"#FFFFFF",fontFamily:Style.main.fonts.fontOpenSans ,fontSize:10}} >{item.showtime}</Text>
		return (
		  <View style={{backgroundColor:'#BBBBBB' , width : Style.main.screenWidth*0.8-20 , borderBottomLeftRadius:10 , borderBottomRightRadius:10,justifyContent:'center' , alignItems:'center' }}>
			
				<View style={{width : Style.main.screenWidth*0.8-20 , height:Style.main.screenWidth*0.8-20 , marginBottom : 10}}>
					<Image 
						source={{uri : "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data="+section.ticcode }}			
						style={{ width: '100%', height: '100%'}}
					></Image>
				</View>
				
				<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,fontWeight:'bold',color:'#333333'}}>Your Booking Code</Text>
				<View style={{width : (Style.main.screenWidth*0.8-20)*0.8 , justifyContent:'center' , alignItems:'center'}}>
					<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , marginBottom : 10}}>{section.ticcode}</Text>
				</View>
				
				<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,fontWeight:'bold',color:'#333333'}}>Cinema Address</Text>
				<View style={{width : (Style.main.screenWidth*0.8-20)*0.8 , justifyContent:'center' , alignItems:'center'}}>
					<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333' , marginBottom : 10 , textAlign:'center' }}>{section.cinema.address}</Text>
				</View>
				
				<View style={{width : Style.main.screenWidth*0.8-20 , marginBottom : 10 , flexDirection : 'row'  , justifyContent:'center' , alignItems:'center' }}>
					
					<View style={{flex:0.3,justifyContent:'center' , alignItems:'center'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,fontWeight:'bold',color:'#333333'}}>Studio</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333'}}>{section.studio}</Text>
					</View>
					
					<View style={{flex:0.3,justifyContent:'center' , alignItems:'center'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,fontWeight:'bold',color:'#333333'}}>Seat</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333'}}>{section.chair}</Text>
					</View>
				
					<View style={{flex:0.3,justifyContent:'center' , alignItems:'center'}}>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:14,fontWeight:'bold',color:'#333333'}}>Price</Text>
						<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:12,color:'#333333'}}>Rp. {this.moneyToCurency(section.price)}</Text>
					</View>
				</View>
		  </View>
		);
	  };
	render() {
		console.log(this.state.tickets);
		
		
		
		return (
		  <View style={{ flex: 1 }}>
			<ScrollView>
				<View style={{justifyContent:'center' , alignItems:'center' , width : Style.main.screenWidth,marginTop:30 , marginBottom : 30}}>
					<Text style={{fontFamily:Style.main.fonts.fontOpenSans,fontSize:18,fontWeight:'bold',color:'#333333'}}>Your Tickets</Text>
				</View>
				<View style={{justifyContent:'center' , alignItems:'center' , width : Style.main.screenWidth,marginBottom : 30}}>
					<Accordion
						sections={this.state.tickets }
						touchableComponent={TouchableOpacity}
						activeSections={this.state.activeSections}
						renderHeader={this._renderHeader}
						renderContent={this._renderContent}
						onChange={this._updateSections}
						style={{justifyContent: "center", alignItems: "center" }}
					/>
				</View>
			</ScrollView>
		  </View>
		);
	}
}

export default Tickets