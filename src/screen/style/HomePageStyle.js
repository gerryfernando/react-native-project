import {StyleSheet} from 'react-native'
import ApplicationStyles from './MainStyle'

export default { 
	main : ApplicationStyles,
	styleSheets : StyleSheet.create({
		fullScreen : {width: '100%', height: '101%'},
	})
}