import {StyleSheet} from 'react-native'
import ApplicationStyles from './MainStyle'

export default { 
	main : ApplicationStyles,
	styleSheets : StyleSheet.create({
		fullScreen : {width: '100%', height: '101%'},
		styleActiveTab : {borderRadius:15,
									backgroundColor:'#FD6C57',
									width:ApplicationStyles.screenWidth*0.35,
									height:'100%',
									marginLeft:5,
									marginRight:5},
		styleUnactiveTab : {borderRadius:15,
									backgroundColor:'#00000000',
									width:ApplicationStyles.screenWidth*0.35,
									height:'100%',
									marginLeft:5,
									marginRight:5,
									borderWidth: 3,
									borderColor: '#FD6C57'}
	})
}