import {StyleSheet} from 'react-native'
import ApplicationStyles from './MainStyle'

export default { 
	main : ApplicationStyles,
	styleSheets : StyleSheet.create({
		fullScreen : {width: '100%', height: '101%'},
		styleActiveTab : {borderRadius:15,
									backgroundColor:'#FD6C57',
									width:ApplicationStyles.screenWidth*0.2,
									height:'100%',
									marginLeft:10},
		styleUnactiveTab : {borderRadius:15,
									backgroundColor:'#2D3E50',
									width:ApplicationStyles.screenWidth*0.2,
									height:'100%',
									marginLeft:10,
									borderWidth: 3,
									borderColor: '#4BBF9E'},
		styleDayActive : {justifyContent:'center' , alignItems:'center',borderWidth : 2,borderColor:'#FD6C57' ,backgroundColor:'#FD6C57',borderBottomRightRadius: 5 ,borderBottomLeftRadius: 5, height:'50%' , width:'100%'},
		styleDayUnactive : {justifyContent:'center' , alignItems:'center',borderWidth : 2,borderColor:'#FD6C57',backgroundColor:'#E9798000' ,borderBottomRightRadius: 5 ,borderBottomLeftRadius: 5, height:'50%' , width:'100%'},
		styleActiveTitle : {color:'#FFFFFF',fontSize:12,fontWeight:'bold',fontFamily:ApplicationStyles.fonts.fontOpenSans},
		styleUnactiveTitle : {color:'#4BBF9E',fontSize:12,fontWeight:'bold',fontFamily:ApplicationStyles.fonts.fontOpenSans},
		headerDetail : {backgroundColor:'#2D3E50',width:ApplicationStyles.screenWidth,height:100,overflow: 'visible',flexDirection: 'row',
								borderTopLeftRadius:20,borderTopRightRadius:20 , marginTop:-20}
	})
}