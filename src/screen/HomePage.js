import React, { Component } from 'react';

import { Text ,Picker, View, BackHandler,Alert, Dimensions, ScrollView,TouchableOpacity,StatusBar,DrawerLayoutAndroid } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-snap-carousel';
import Style from './style/HomePageStyle';
import SideMenu from './subscreen/sidemenu/SideMenu';
import GuestSideMenu from './subscreen/sidemenu/GuestSideMenu';
import Sidebar from "react-native-side-menu";
import { StackNavigator} from  'react-navigation';
import { Image ,Icon,Button} from 'react-native-elements';
import Images from '../library/images';
import ListMovie from './subscreen/home/ListMovie';
import CinemakuyAPI from '../library/CinemakuyAPI';




class HomePage extends Component {
	

	static navigationOptions=({ navigation })=>{
		
		const cities = ["Bali","Balikpapan","Bandung","Banjarbaru","Batam","Batu","Bekasi","Binjai","Blitar","Bogor","Cikarang","Cirebon","Depok","Duri","Gresik","Jakarta","Jambi","Jember","Karawang","Kediri","Kendari","Ketapang","Kupang","Lampung","Madiun","Makassar","Malang","Mamuju","Manado","Mataram","Medan","Mojokerto","Palembang","Pekanbaru","Ponorogo","Prabumulih","Probolinggo","Purwakarta","Purwokerto","Sampit","Semarang","Serang","Solo","Surabaya","Tangerang","Tegal","Yogyakarta"];
		let arrPicker = [];
		{cities.map((val) => {
			arrPicker.push(<Picker.Item key={val} label={val} value={val}/>)
		})}
		
		return {
			headerStyle: {
				backgroundColor: '#3B506B',
				height:60,color : 'blue'
			  },
			headerRight:(
				<View  style={{  backgroundColor:"#fd6c57", borderRadius:10 , marginRight:10}}>
					<Picker
					  selectedValue={navigation.getParam('currentCity')}
					  style={{height: 40, width: 150}}
					  //itemStyle={{ backgroundColor: "#123132", color:'#FFFFFF', fontFamily:"Ebrima", fontSize:10 }}
					  onValueChange={navigation.getParam('onChangeCity')}
					  //itemTextColor="#FFFFFF"
					  >
					  {arrPicker}
					</Picker>
				</View>
			),
			headerLeft:(
				<TouchableOpacity onPress={navigation.getParam('openDraw')}>
                    <Icon
						name='menu'
						color='#fd6c57'
						iconStyle = {{marginLeft:10}}
						
					/>
                </TouchableOpacity>
			
			  
			)
		}
		
		/*
		headerRight:<Button title = "Home"/>,
		headerLeft:{{icon='menu'}}*/
	};
	
	

	constructor(props) {
	  super(props);
	  // Don't call this.setState() here!
	  
	  this.navigate = props.navigation;
	  this.handleBackPress = this.handleBackPress.bind(this);
	  this.state = {
		  sidebarOpen: true
		};
		this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
	  this.updateIndex = this.updateIndex.bind(this);
	  this.getFilmNowPlaying = this.getFilmNowPlaying.bind(this);
	  this.gotoDetail = this.gotoDetail.bind(this);
	  //this.backHandler = this.backHandler.bind(this);
	  this.handleBackPress = this.handleBackPress.bind(this)
	  
	}
	
	getFilmNowPlaying(city){
		
		CinemakuyAPI.getMovieNowPlaying(city , (response)=>{
			console.log("Data Get Finish");
			this.movie = response;
			this.setState({corIdx : 0})
		});
	}
	
	_onChangeCity = (selectedCity) => {
		AsyncStorage.setItem('movies/city', selectedCity);
		console.log(selectedCity);
		if(selectedCity==null){
			selectedCity = "Bali";
		}
		if(this._carousel!=null){
			this._carousel.snapToItem(0,false);
		}
		this.movie = null;
		this.getFilmNowPlaying(selectedCity)
		this.props.navigation.setParams({ currentCity: selectedCity });

	}
	
	componentDidMount() {
		
		
		this.subs = [
		  this.props.navigation.addListener('didFocus', ()=>this.componentDidFocus(this.handleBackPress)),
		  this.props.navigation.addListener('willBlur', ()=>this.componentWillBlur(this.handleBackPress)),
		];
		
		this.props.navigation.setParams({ onChangeCity: this._onChangeCity });
		this.props.navigation.setParams({ openDraw: this.onSetSidebarOpen });
		this.props.navigation.setParams({ currentCity: "Bandung" });
		//this.props.navigation.setParams({ currentCity: "Bandung" });
		AsyncStorage.getItem('movies/city',(err, result) => {
			this.props.navigation.setParams({ currentCity: result });
			this._onChangeCity(result);
		} )
		
		
		//console.log(this.handleBackPress)
		return true
	}
	
	getAccount(username,token){
		//console.log("masuk1");
		CinemakuyAPI.GetAccount({username,token} , (response)=>{
			console.log("Data Get Finish");
			console.log(response);
			//this.setState({udata : response});
			this.setState({
				udata : response,
				userAccountData : {
					username:username,
					token:token,
					name : response.name,
					phone : response.phone,
					credit : response.credit
			}
			});
		});
	}
	
	
	
	componentDidFocus(events){
		console.log("Screen focused")
		
		BackHandler.addEventListener('hardwareBackPress', events);
		
		AsyncStorage.getItem('account/username',(err, result) => {
			username = result;
			AsyncStorage.getItem('account/token',(err, result) => {
				token = result;
				//console.log(username,token)
				//this.setState({udata:{}});
				this.getAccount(username,token);
			} )
		})
		
		//console.log("BackHandler Event Added")
		
	}
	componentWillBlur(events){
		console.log("Screen is not focused")
		//this.backHandler.remove()
		BackHandler.removeEventListener('hardwareBackPress', events);
		//console.log("BackHandler Event Removed")
	}

	componentWillUnmount() {
		this.subs.forEach(sub => sub.remove());
		
		return true;
		
	}
	isFocused = true
	handleBackPress() {
		
		if(this.state.udata==null||this.state.udata.username == null||this.state.udata==""||this.state.udata.username==""){
			this.props.navigation.pop();
			return true;
		}
		//console.log("this is backpress");
		Alert.alert("Konfirmasi Log out","Yakin akan keluar dari akun ini?",
			[
				{text: 'Ya', onPress: () => {console.log('Ya pressed'); this.resetDataAccount(); this.props.navigation.navigate('Login');},style: 'cancel'},
				{text: 'Tidak', onPress: () => {console.log('No Pressed'); }},
			],
			{cancelable: true}
		);
		return true;

	}
	
	state = {
		udata:null,
		corIdx : 0
	}
	movie = null
	
	resetDataAccount(){
		AsyncStorage.setItem('account/token', "");
		AsyncStorage.setItem('account/username', "");
	}
	
	isDrawerOpen = false
	
	onSetSidebarOpen() {
		console.log("run");
		//this._drawer.open("left")
		//this.setState({ isOpen: (this.state.isOpen==false) });
		//console.log(this.refs['DRAWER_REF'].onDrawerOpen)
		if(this.refs['DRAWER_REF'] == null){
			return;
		}
		
		if(!(this.isDrawerOpen)){
			this.refs['DRAWER_REF'].openDrawer();
			
		}else{
			this.refs['DRAWER_REF'].closeDrawer();
		}
	  }
	renderLeftSidebar(){
		return (< SideMenu />)
	}
	renderRightSidebar(){
		return (< SideMenu />)
	}
	
	updateIndex(){
		console.log(this._carousel.currentIndex);
		if(this.state.corIdx == this._carousel.currentIndex){
			return;
		}
		
		this.setState({corIdx : this._carousel.currentIndex});
		
		
	}
	
	_renderItem ({item, index}) {
		
		console.log("Render Corosel List HomePage");
		
        return (
            <View  style={{ flex:1, backgroundColor:'#000000'}} >
					<Image 
						source={{uri : item.image}}
						style={{ width: '100%', height: '100%'}}
					>
					</Image>
					
            </View>
        );
    }
	
	gotoDetail(idx){
		console.log(this.movie.data[idx]);
		this.props.navigation.navigate('Detail' , {data:this.movie.data[idx] , city:(this.props.navigation.getParam('currentCity') || 'Bali')});
	}
	/*AsyncStorage.multiGet(['account/token','account/username'], (err, result) => {
			if(result[0][1] !="" && result[1][1]!=""){
				console.log("Check Account Login Status");
				navigationView=null;
				navigationView = (
					<View style={{flex: 1, backgroundColor: '#fff'}}>
					  <SideMenu nav={this.props.navigation}/>
					</View>
					);
				
			}
		});
		  */
		  
	render() {
		let username,token;
		//console.log(this.state.udata)
		
		if(this.state.udata==null|| this.state.userAccountData==null){
			var navigationView = (
					<View style={{flex: 1, backgroundColor: '#fff'}}>
					  <GuestSideMenu nav={this.props.navigation}/>
					</View>
				);
		}else{
			//console.log(this.state.userAccountData)
			var navigationView = (
					<View style={{flex: 1, backgroundColor: '#fff'}}>
					  <SideMenu nav={this.props.navigation} udata={this.state.udata} userAccountData={this.state.userAccountData}/>
					</View>
					);
			
		}
		
		
		 
		

		console.log("Render");
		let buttonsListArr = [];
		let dataRecommendedMovie = []
		for(let i = 0; i < (this.movie==null?0:this.movie.data.length); i++){
			this.movie.data[i].real = i;
			buttonsListArr.push(<ListMovie key={i} data={this.movie.data[i]} onPress={(data)=> this.gotoDetail(data.real) } />);
			if(this.movie.data[i].recommended){
				dataRecommendedMovie.push(this.movie.data[i]);
				
			}
		}
		
		
		
		if(this.movie == null){
			return (<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}><Text>Loading...</Text></View>)
		}else{
			let recommended;
			let recommended2;
			if(dataRecommendedMovie.length > 0){
				recommended = (<View style={{width:Style.main.screenWidth, height:Style.main.screenHeight/3.7, marginTop:Style.main.screenHeight/20,marginBottom:Style.main.screenHeight/20 }}>
									<Carousel
									  ref={(c) => { this._carousel = c; }}
									  onSnapToItem={this.updateIndex}
									  data={dataRecommendedMovie}
									  renderItem={this._renderItem}
									  sliderWidth={Style.main.screenWidth}
									  itemWidth={Style.main.screenWidth/3}
									  containerCustomStyle={{ flex: 1 }}
									  slideStyle={{ flex: 1 }}
									  inactiveSlideOpacity={0.65}
									  inactiveSlideScale={0.7}
								
									/>
								</View>)
				recommended2 = (<View style={{width:Style.main.screenWidth, justifyContent: "center", alignItems: "center",marginBottom:30, marginTop:10 }}>
									
									<View style={{position:'absolute',borderRadius:10,backgroundColor:'#34526E',width:Style.main.screenWidth/2, padding:10, alignItems: "center"}}>
										
												<Text style={{color:'#41C3AB',textAlign: 'center',fontSize: 12,fontWeight:'bold',fontFamily:Style.main.fonts.fontOpenSans,width:'100%',height:'100%'}}
												onPress={()=> { this.gotoDetail(dataRecommendedMovie[this.state.corIdx].real) }}>
													{(this.movie==null?null : this.movie.data[this.state.corIdx].name)}
												</Text>
										
									</View>

								</View>)
			}
								
			return (
			

		   <DrawerLayoutAndroid
			  drawerWidth={Style.main.screenWidth*(6/7)}
			  ref={'DRAWER_REF'}
			  onDrawerClose = {()=>{this.isDrawerOpen=false; }}
			  onDrawerOpen = {()=>{this.isDrawerOpen=true; }}	
			  drawerPosition={DrawerLayoutAndroid.positions.Left}
			  renderNavigationView={() => navigationView}>
			  
		
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>

			<StatusBar
                backgroundColor='#3B506B'
            />
			<ScrollView>
				<View style={{
						position:'absolute',
						top:0,
						flex:1,
						width:'100%',
						height:(Style.main.screenHeight/4)*2,
				}}>
					<Image 
						source={{uri : (this.movie == null ? null : this.movie.data[this.state.corIdx].image) }}
						style={{ width: '100%', height: '100%'}}
						blurRadius={3}
					>
					</Image>
				</View>
				
				<View style={{
						position:'absolute',
						top:'-1%',
						flex:1,
						width:'100%',
						height:(Style.main.screenHeight/4)*2,
				}}>
					<Image 
						source={Images.layerFade}
						style={{ width: '100%', height: '104%'}}
						
					>
					</Image>
				</View>
				
				{recommended}
				{recommended2}
				<View style={{	width:Style.main.screenWidth, justifyContent: "center", 
								alignItems: "center" , backgroundColor:'#EDF1ED', 
								borderTopLeftRadius:20,borderTopRightRadius:20 ,
								paddingTop : 30,height:30}}>
					
				</View>
				<View style={{	width:Style.main.screenWidth, justifyContent: "center", 
								alignItems: "center" , backgroundColor:'#EDF1ED'}}>
					{buttonsListArr}
				</View>

			</ScrollView>
				
		  </View>
		  </DrawerLayoutAndroid>
		  )
		}
	}
}

export default HomePage